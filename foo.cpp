#include "foo.h"

/*
 * we keep rows and columns separate for later calculation
 * of solutions for retangular boards. For the code below
 * we just assume m = n
 */

foo::foo(long columns, long rows)
    : board (columns, std::vector<int>(rows, ' '))
{
    m = columns;
    n = rows;
}

long foo::finder(long x, long y)
{
    for (auto i = x; i < m; i++)
    {
        if(board[i][y]==' '){
            return i;
        }
    }
    return -1;
}

void foo::placeQ(long x, long y)
{
    //////////////////////
    //marks all attacked rows, columns and diagonals for Q placed in (x,y)
    //////////////////////

    // columns and rows
    for (auto i = 0ul; i < m; ++i)
    {
        board[x][i]='+';
        board[i][y]='+';
    }

    /* this is the diagonal [\]
       m - |x-y| gives the number of elements in the diagonal
       if y>x, the point is above the diagonal and any diagonal begins at (x-y,0)
       if x>y, it is below and any diagonal begins at (0,y-x)
       else both coordinates just get equal values */
    auto ab = std::labs(x-y);

    for (auto i = 0ul; i < m-ab; ++i)
    {
        board[(x>y)*ab+i][(y>x)*ab+i]='+';
    }

    /* this is the diagonal [/]
       x+y+1 gives the number of elements in the diagonal when the point is above the diagonal
       2*m-1-(x+y) is the number of elements when it is below the diagonal
       if x+y=<m-1, the point is above, or is at, the diagonal and any diagonal begins at (0,x+y)
       if x+y>m-1, it is below and any diagonal begins at (x+y-(m-1),m-1) */
    auto s = ((x+y)>(m-1))*((m+n-1)-x-y) + ((x+y)<m)*(x+y+1);

    for (auto i = 0ul; i < s; ++i)
    {
        board[(m-1)*((x+y)>(m-1))+((x+y)<m)*(x+y)-i][(x+y-(m-1))*((x+y)>(m-1))+i]='+';
    }

    // mark the position where the Q is placed;
    board[x][y]= 'Q';
}

foo::vect2D foo::solver(long c)
{
    placeQ(c,0);
    std::vector<vect2D> stack;
    stack.push_back(board);

    std::vector<int> lastQ;
    vect2D solutions;

    lastQ.push_back(c);

    auto y = 1ul;
    auto x = 0ul;

    for( ; ; )
    {
        while ( y < n )
        {
            while ( x < m )
            {
                if ( board[x][y] == ' ' ){
                    placeQ(x,y);
                    stack.push_back(board);
                    lastQ.push_back(x);
                    ++y;
                    x = -1;
                }
                ++x;
            }
            //////////////////
            if ( y < n ){
                do {
                    --y;
                    if (y == 0){
                        return solutions;
                    }
                    stack.pop_back();
                    board.clear();
                    board = stack.back();
                    x = finder( lastQ.back()+1, y );
                    lastQ.pop_back();
                } while ( x == -1 );
            }
        }

        solutions.push_back(lastQ);

        stack.pop_back();
        board.clear();
        board = stack.back();
        y = n - 1;
        x = lastQ[y]+1;
        lastQ.pop_back();
    }
}
