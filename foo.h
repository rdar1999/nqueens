#ifndef FOO_H
#define FOO_H

#include <vector>
#include <iostream>

class foo
{
public:
    typedef std::vector<std::vector<int>> vect2D;

    foo(long columns, long rows);

    vect2D board;

    void placeQ(long x, long y);
    long finder(long x, long y);
    vect2D solver(long c);

    long m;
    long n;

};

#endif // FOO_H
