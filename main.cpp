#include <chrono>
#include "foo.h"


int main()
{
    int sum=0;
    std::chrono::steady_clock t;

    auto start = t.now();

    for (auto i=0; i<8; ++i)
    {
        auto p = new foo(8,8);
        auto vectQueens = p->solver(i);
        p->board.clear();
        sum += vectQueens.size();
        delete p;
        std::cout << "------------------" << vectQueens.size() << "\n";
    }

    auto end = t.now();

    auto time = static_cast<std::chrono::duration<double>>(end - start);

    std::cout << "TOTAL ------------" << sum << " - "<< time.count()<< "s\n";

    return 0;
}
